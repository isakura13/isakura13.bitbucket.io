'use strict'
let input = document.querySelector("#enter");
let button = document.querySelector("#enter_button");
let result_el = document.querySelector("#result ");

button.onclick = () => {
    try {
        let result = eval(input.value); //пробуем, если все будет корректно, тогда catch не сработает
        result_el.innerHTML = result;
    } catch (error) {
        console.error(error.name);
        result_el.innerHTML = "Вы что-то не то ввели, молодой человек<br> Подумайте еще раз";
        //можно пользователю объяснять, что он не прав, если он допустил ошибку
        //хотя естественно пользователю лучше не давать эту возможность))
    }
}