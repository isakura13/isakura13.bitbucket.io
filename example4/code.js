var headers = ['Сортировка простыми обменами, сортировка пузырьком',
    'Двоичный поиск', 'Жадный алгоритм'];
var paragraps = ['Для понимания и реализации этот алгоритм — простейший, но эффективен он лишь для небольших массивов.', '  Классический алгоритм поиска элемента в отсортированном массиве (векторе), использующий дробление массива на половины.', 'Алгоритм, заключающийся в принятии локально оптимальных решений на каждом этапе, допуская, что конечное решение также окажется оптимальным.'];
//у нас тут и примеры, и алгоритмы немного вспомним
//Итак, довольно таки простая задача: пускай из наших массивов у нас генерируются наши статьи
let root = document.getElementById("root"); // по привычке использую root
let article__btn = document.querySelector(".article__btn");

article__btn.onclick = () => {
    for (let i = 0; i < headers.length; i++) {
        root.insertAdjacentHTML("beforeend", `
    <div class="content is-medium">
        <h1>${headers[i]} </h1>
        <p>${paragraps[i]}</p>
    </div>`)
    }
}