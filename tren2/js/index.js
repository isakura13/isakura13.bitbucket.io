import anime from "./anime.es";
import getRandomInt from "./random";
import showResult from "./showResult";

anime({
  targets: ".anim-elem",
  translateX: [-50, 50],
  easing: "linear",
  direction: "alternate",
  duration: 1000,
  loop: true
});

function get_data() {
  fetch(
    "https://gist.githubusercontent.com/isakura313/b705fd423e996a56b35b18b876458f18/raw/48023a7ffa598585f80303557e68b2011f776849/main.json"
  )
    // я переложил информацию в json файл - это показалось мне более логичным,
    // если кто-то захочет замасштабировать эту игру на сервер
    .then(res => res.json())
    .then(data => {
      //асинхронно вызываю функцию основной игры, когда загрузится необходимая информация
      read_data(data); //вызов основной функции игры
    })
    .catch(err => {
      console.warn("произошла ошибка");
      console.warn(err.name);
    });
}

get_data(); // логично это обернуть в самовызывающийся модуль, но... для упрощения я не буду этого делать

async function read_data(information) {
  console.table(information); // просто выводим нашу информацию. Нужно удалить потом в продакшене
  var number_of_level = 0; // по дефолту нас будет запускаться уровень номер 0
  var error_sound = new Audio("sounds/error_sound.wav");
  var fail_sound = new Audio("sounds/fail_sound.wav"); // играется при проигрыше
  var press_sound = new Audio("sounds/press_sound.wav");
  var succes_sound = new Audio("sounds/succes_sound.wav"); // думаю из названий все в приниципе понятно. Вот этот играется при переходе на некст левел

  //получаем все звуки, которые нам нужны
  let modal = document.querySelector(".modal"); // наше модельное окно с результатами
  var target_error = document.querySelector(".target_error"); // элемент в которое мы будем писать историю наших ошибок
  let error_panel = document.querySelector(".error-panel"); //панель прогресса наших ошибок
  let begin = document.querySelector(".begin"); // здесь у нас надпись, которая приглашает пользователя нажать enter для начала игры. Потом она у нас должна пропасть
  let progress = document.getElementById("prog"); // здесь прогресс ошибок пользователя
  let buttons = document.querySelector(".buttons"); // элемент в который мы будем писать наши буковки
  let name_level = document.querySelector(".name-level"); //сюда мы будем писать название нашего уровня
  let modal_close = document.querySelector(".modal-close"); // кнопка, при клике на которую у нас будет у нас будет закрываться модальной окно с результами

  document.addEventListener("keydown", StartGame, {
    once: true
    //благодаря once у нас отрисовка вызывается только один раз при загрузку страницы
  });

  function StartGame(e) {
    if (e.key == "Enter") {
      error_panel.classList.remove("is-hidden"); //делаем видимой нашу панель ошибок
      press_sound.play();
      begin.remove(); // удаляем приглашающую надписась
      mainGame(); // осноная функция игры
    }
  }

  function drawBoard(info) {
    let str_arr = info.level_info[number_of_level].symbols;
    name_level.innerHTML = info.level_info[number_of_level].name_level;
    let col_arr = info.symbol_colors;
    for (let i = 0; i < 20; i++) {
      let rand = getRandomInt(str_arr.length);
      buttons.insertAdjacentHTML(
        "afterbegin",
        `<button class='game-button button 
                    is-large ${col_arr[rand]}' id='${str_arr[rand]}'>
                        ${str_arr[rand]}</button>`
      );
    }
  }

  function mainGame() {
    drawBoard(information);
    document.addEventListener("keydown", press); //  я создал отдельную функцию, что бы была возможность ее останавливать
  }

  var errors_count = 0;
  var count_right = 0;

  function press(e) {
    let elements_arr = document.querySelectorAll(".game-button"); // выбираем массив всех созданных элементов
    if (e.key == elements_arr[0].id) {
      // здесь можно выбирать и по querySelector, но тогда код будет длиннее
      elements_arr[0].remove();
      count_right++; //  считаем правильные ответы
      press_sound.play();
    } else {
      errors_count++; // считаем ошибки
      error_sound.play();
      progress.value = errors_count;
      if (errors_count > 20) {
        // если пользователь допустит ошибок больше чем у нас букв, игра закончится
        number_of_level = 0;
        fail_sound.play();
        alert("game over");
        setTimeout(() => {
          window.location.reload();
        }, 2000);
        //при проигрыше  выводится грустный звук и через 2 секунды страница перезгружается
      }
    }
    if (count_right == 20) {
      count_right = 0;
      number_of_level++;
      if (number_of_level == 3) {
        modal.classList.add("is-active");
        showResult(target_error, errors_count);
        modal_close.onclick = function() {
        modal.classList.remove("is-active");
        window.location.reload();
        };
      }
      mainGame();
      succes_sound.play();
      let win = confirm("Хотите поиграть еще?");
    }
  }
}
